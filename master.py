from balancer import Balancer
from server import all_servers
from exceptions import BroadcastNotFoundException
from server_pool import ServerPool

import paramiko
import time
from threading import Lock, Thread
from flask import Flask, request, Response
from functools import partial
from gevent.wsgi import WSGIServer


def run_balancer_app(master):
    app = Flask(__name__)
    upd = partial(update_server, master=master)
    app.add_url_rule('/update/<int:broadcast_id>/<int:ctid>', 'update_server', upd, methods=['POST'])
    http_server = WSGIServer(('', master.port), app)
    http_server.serve_forever()


def update_server(master, broadcast_id, ctid):
    data = int(request.data)
    master.update_server(broadcast_id, ctid, request.environ['REMOTE_ADDR'][7:], data)
    return Response("OK", status=200)


class Master:
    def __init__(self, token, base, location, key_ids, key_path, host, port, max_load=100 * 1048576,
                 min_free_load=100 * 1048576, client_load=100 * 1024, initial=1, step=1):
        self.token = token
        self.base = base
        self.location = location
        self.key_ids = key_ids
        self._counter = 0
        self.address = host + ':' + str(port)
        self.port = port
        self.key = paramiko.RSAKey.from_private_key_file(key_path)

        self.client_load = client_load
        self.min_free_load = min_free_load
        self.max_load = max_load
        self.initial = initial
        self.step = step

        self._server_pool = ServerPool()
        self._broadcasts = dict()
        self._lock = Lock()
        self._counter = 0

        Thread(target=self._start_monitor_server).start()

        for server in all_servers(token):
            if server.name == 'live_relay':
                print('added server ' + server.address())
                num = self._add_server(server)
                if num is not None:
                    self._counter = max(self._counter, num + 1)

    def _start_monitor_server(self):
        run_balancer_app(self)

    def _add_server(self, server):
        c = paramiko.SSHClient()
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        flag = True

        server.wait()
        server.refresh()

        while flag:
            try:
                c.connect(hostname=server.address(), username='root', pkey=self.key)
                flag = False
            except paramiko.ssh_exception.NoValidConnectionsError:
                time.sleep(3)
        _, stdout, _ = c.exec_command("cat /root/server/info")
        info = stdout.readlines()[0].split()
        c.close()

        server.creation_time = float(info[1])
        if info[0] == 'False':
            self._server_pool.add(server)
            return

        num = int(info[2])
        if num not in self._broadcasts:
            self._broadcasts[num] = Balancer(num, self.token, self.base, self.location,
                                             self.key_ids, self.key, [info[3], info[4]], self.address,
                                             self._server_pool, self.max_load, self.min_free_load, self.client_load,
                                             0, self.step)
        self._broadcasts[num].free_load += self.max_load
        self._broadcasts[num].claim_server(server)
        return num

    def update_server(self, broadcast_id, ctid, addr, load):
        return self._broadcasts[broadcast_id].update_server(ctid, addr, load)

    def add_broadcast(self, address):
        with self._lock:
            self._broadcasts[self._counter] = (Balancer(self._counter, self.token, self.base, self.location, self.key_ids,
                                                        self.key, address, self.address, self._server_pool, self.max_load,
                                                        self.min_free_load, self.client_load, self.initial, self.step))
            self._counter += 1
            ans = self._counter - 1
        return ans

    def delete_broadcast(self, broadcast_id):
        if broadcast_id not in self._broadcasts:
            raise BroadcastNotFoundException
        with self._lock:
            Thread(target=self._broadcasts[broadcast_id].delete).start()
            del self._broadcasts[broadcast_id]

    def get_address(self, broadcast_id, stream_type):
        if broadcast_id not in self._broadcasts:
            raise BroadcastNotFoundException
        return self._broadcasts[broadcast_id].add_user(stream_type)

    def get_status(self, broadcast_id):
        if broadcast_id not in self._broadcasts:
            raise BroadcastNotFoundException
        return self._broadcasts[broadcast_id].status
