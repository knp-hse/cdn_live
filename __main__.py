from master import Master
from api_server import get_api_app
import configparser
from gevent.wsgi import WSGIServer
import os


def main():
    with open(".pid", "w") as f:
        f.write(str(os.getpid()))
    config = configparser.ConfigParser()
    config.read('conf')

    with open(config['security']['tokens_file'], 'r') as f:
        tokens = [x.strip() for x in f if x.strip() != '']

    rules = dict()
    for key in config['rules']:
        rules[key] = int(config['rules'][key])
    master = Master(config['vscale']['token'], config['vscale']['image'], config['vscale']['location'],
                    [int(config['security']['key_id'])], config['security']['key_file'], config['network']['hostname'],
                    int(config['network']['balancer_port']), **rules)

    app = get_api_app(master, tokens)
    http_server = WSGIServer(('', int(config['network']['api_port'])), app)
    http_server.serve_forever()


if __name__ == '__main__':
    main()
