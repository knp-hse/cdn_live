import time
from threading import Lock, Thread


class ServerPool:
    def __init__(self):
        self._free_servers = []
        self._lock = Lock()
        self._active = True
        Thread(target=self._monitor).start()

    def delete(self):
        self._active = False

    def _monitor(self):
        while self._active:
            time.sleep(120)
            with self._lock:
                now = time.time()
                i = 0
                while i < len(self._free_servers):
                    if 3600 - ((now - self._free_servers[i].creation_time) % 3600) < 300:
                        self._free_servers[i].delete()
                        del self._free_servers[i]
                    else:
                        i += 1

    def add(self, server):
        if 3600 - ((time.time() - server.creation_time) % 3600) < 300:
            server.delete()
            return
        with self._lock:
            self._free_servers.append(server)

    def get(self):
        with self._lock:
            if len(self._free_servers) > 0:
                ans = self._free_servers[-1]
                del self._free_servers[-1]
                return ans
            else:
                return None
