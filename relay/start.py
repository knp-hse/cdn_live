#!/usr/bin/env python3
import sys
import os
import time

with open('/root/server/default', 'r') as f:
    content = f.read()


hls_split = sys.argv[2].split('/')
origin = [sys.argv[1], '/'.join(hls_split[:3]), '/' + '/'.join(hls_split[3:-1])]
content = content.replace('_STREAM_ORIGIN_RTMP', origin[0]).replace('_STREAM_ORIGIN_HLS', origin[1])\
    .replace('_STREAM_PATH_HLS', origin[2])
with open('/usr/local/nginx/conf/nginx.conf', 'w') as f:
    f.write(content)

os.system('/usr/local/nginx/sbin/nginx')

with open('/root/server/info', 'w') as f:
    f.write('True' + ' ' + str(time.time()) + ' ' + sys.argv[3] + ' ' + origin[0] + ' ' + sys.argv[2])

with open("/root/server/master", 'w') as f:
    f.write(sys.argv[4])
