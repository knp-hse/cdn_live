#!/usr/bin/env python3
import subprocess
import requests
import time


with open('/root/server/master', 'r') as f:
    master = f.read().strip()

while True:
    tx_bytes = int(subprocess.run(['cat /sys/class/net/ens3/statistics/tx_bytes'],
                                  stdout=subprocess.PIPE, shell=True).stdout.decode('utf-8'))
    requests.post(master, data=str(tx_bytes))
    time.sleep(15)
