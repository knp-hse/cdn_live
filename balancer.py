from server import Server
import paramiko
from threading import Lock, Thread
import time


class Balancer:
    def __init__(self, num, token, base, location, key_ids, key, origin, address, server_pool, max_load=100*1048576,
                 min_free_load=100*1048576, client_load=100*1024, initial=1, step=1):
        self.num = num
        self.token = token
        self.base = base
        self.location = location
        self.key_ids = key_ids
        self.step = step
        self.address = address
        self.key = key
        self.server_pool = server_pool
        self.active = True

        if not origin[0]:
            origin[0] = 'rtmp://0.0.0.0'
        if not origin[1]:
            origin[1] = 'http://0.0.0.0/s'
        self.origin = origin
        self._hls_name = origin[1].split('/')[-1]

        self.client_load = client_load
        self.min_free_load = min_free_load
        self.max_load = max_load
        self.free_load = 0

        self._server_lock = Lock()

        self.status = 'creating'
        self._servers = []
        self._add_servers(initial)

        Thread(target=self._check_servers).start()

    def _add_server(self):
        s = self.server_pool.get()
        if not s:
            s = Server(self.token, 'live_relay', self.base, self.location, self.key_ids)
            s.create(do_start=True)
            s.wait()
            s.refresh()

        self.claim_server(s)

    def claim_server(self, server):
        c = paramiko.SSHClient()
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        flag = True

        server.wait()
        server.refresh()
        counter = 0

        while flag:
            if counter > 150:
                server.load_image(self.base)
                server.wait()
                server.start()
                server.wait()
                counter = 0
            try:
                c.connect(hostname=server.address(), username='root', pkey=self.key)
                flag = False
            except paramiko.ssh_exception.NoValidConnectionsError:
                time.sleep(3)
                counter += 3
        c.exec_command("killall python3")
        c.exec_command("/usr/local/nginx/sbin/nginx -s quit")
        c.exec_command("python3 /root/server/start.py %s %s %d %s/update/%d/%d" % (self.origin[0], self.origin[1],
                                                                                   self.num, self.address, self.num, server.ctid))
        c.exec_command("setsid python3 /root/server/monitor.py >/root/server/log 2>&1 < /dev/null &")
        c.close()
        self._servers.append(server)
        self.status = 'active'
        if not self.active:
            self._delete_server(0)

    def _add_servers(self, number=-1, wait=False):
        if number < 0:
            number = self.step
        self.free_load += number * self.max_load

        threads = []
        for _ in range(number):
            t = Thread(target=self._add_server)
            t.start()
            threads.append(t)
        if wait:
            for t in threads:
                t.join()

    def _delete_server(self, n):
        server = self._servers.pop(n)
        c = paramiko.SSHClient()
        c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        flag = True

        server.wait()
        server.refresh()

        while flag:
            try:
                c.connect(hostname=server.address(), username='root', pkey=self.key)
                flag = False
            except paramiko.ssh_exception.NoValidConnectionsError:
                time.sleep(3)
        c.exec_command('killall python3')
        c.exec_command("echo 'False %f' > /root/server/info" % server.creation_time)
        c.close()

        self.server_pool.add(server)

    def update_server(self, ctid, address, num):
        success = False
        with self._server_lock:
            i = 0
            while i < len(self._servers):
                s = self._servers[i]
                if s.ctid == ctid:
                    if address == s.address():
                        s.last_update = time.time()
                        success = True
                        self.free_load += s.load
                        if s.total_bytes != 0:
                            s.load = (num - s.total_bytes) / 15
                        self.free_load -= s.load
                        s.total_bytes = num

                        if s.load < 1000 and self.free_load - self.max_load > self.min_free_load:
                            self.free_load -= self.max_load - s.load
                            self._delete_server(i)
                            i -= 1
                        if self.free_load < self.min_free_load:
                            self._add_servers()
                    break
                i += 1
        return success

    def add_user(self, stream_type):
        server = None
        max_load = -1

        with self._server_lock:
            for s in self._servers:
                if max_load < s.load < self.max_load:
                    max_load = s.load
                    server = s

        if server is None:
            return None
        self.free_load -= self.client_load

        addr = server.address()
        if stream_type == 'hls':
            return 'http://' + addr + '/hls/stream/' + self._hls_name
        else:
            return 'rtmp://' + addr + '/rtmp/stream'

    def delete(self):
        self.active = False
        while len(self._servers) > 0:
            self._delete_server(0)

    def _check_servers(self):
        while self.active:
            time.sleep(120)
            now = time.time()
            with self._server_lock:
                for i in range(len(self._servers)):
                    server = self._servers[i]
                    if now - server.last_update > 120:
                        del self._servers[i]
                        self.claim_server(server)
