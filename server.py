import time
import requests
from exceptions import VscaleException


def wait_all(token, ids):
    header = {'X-Token': token,
              'Content-Type': 'application/json'}
    while True:
        flag = False
        r = requests.get('https://api.vscale.io/v1/tasks', headers=header)
        if r.status_code != 200:
            raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])
        for task in r.json():
            if task['scalet'] in ids:
                flag = True
        if not flag:
            break
        time.sleep(3)


def all_servers(token):
    header = {'X-Token': token,
              'Content-Type': 'application/json'}
    r = requests.get('https://api.vscale.io/v1/scalets', headers=header)
    if r.status_code != 200:
        raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])

    ans = []
    for e in r.json():
        keys = [key['id'] for key in e['keys']]
        s = Server(token, e['name'], e['made_from'], e['location'], keys, e['public_address'], e['status'],
                   e['active'], e['ctid'])
        ans.append(s)
    return ans


class Server:
    def __init__(self, token, name, image, location, keys, public_address=None, status=None,
                 active=False, ctid=None, password=None, ):
        self.ctid = ctid
        self.active = active
        self.status = status
        self.public_address = public_address
        self.password = password
        self.keys = keys
        self.image = image
        self.name = name
        self._header = {'X-Token': token,
                        'Content-Type': 'application/json'}
        self.location = location
        self.total_bytes = 0
        self.load = 0
        self.last_update = 0
        self.busy = False
        self.creation_time = 0

    def create(self, do_start=False, rplan='small'):
        if self.ctid is not None:
            raise VscaleException('Server is already created!')
        self.busy = True

        json = {'name': self.name,
                'make_from': self.image,
                'location': self.location,
                'keys': self.keys,
                'rplan': rplan,
                'do_start': do_start
                }
        if self.password is not None:
            json['password'] = self.password

        r = requests.post('https://api.vscale.io/v1/scalets', headers=self._header, json=json)

        if r.status_code == 200:
            self.ctid = r.json()['ctid']
            self.active = False
            self.status = "defined"
            self.creation_time = time.time()
        else:
            raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])

    def delete(self):
        self.busy = True
        r = requests.delete('https://api.vscale.io/v1/scalets/' + str(self.ctid), headers=self._header)
        if r.status_code == 200:
            self.ctid = None
            self.status = None
            self.public_address = None
            self.active = None
        else:
            raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])

    def start(self):
        self.busy = True
        r = requests.patch('https://api.vscale.io/v1/scalets/%d/start' % self.ctid, headers=self._header,
                           json={"id": self.ctid})
        if r.status_code != 200:
            raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])

    def stop(self):
        self.busy = True
        r = requests.patch('https://api.vscale.io/v1/scalets/%d/stop' % self.ctid, headers=self._header,
                           json={"id": self.ctid})
        if r.status_code == 200:
            self.status = "stopped"
        else:
            raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])

    def load_image(self, image):
        self.busy = True
        r = requests.patch('https://api.vscale.io/v1/scalets/%d/stop' % self.ctid, headers=self._header,
                           json={"make_from": image})
        if r.status_code != 200:
            raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])

    def wait(self):
        if self.ctid is None:
            raise VscaleException('Server was not created yet')
        while True:
            flag = False
            r = requests.get('https://api.vscale.io/v1/tasks', headers=self._header)
            for task in r.json():
                if task['scalet'] == self.ctid:
                    flag = True
            if not flag:
                break
            time.sleep(3)
        self.busy = False

    def refresh(self):
        r = requests.get('https://api.vscale.io/v1/scalets/' + str(self.ctid), headers=self._header)
        if r.status_code != 200:
            raise VscaleException(r.headers['VSCALE-ERROR-MESSAGE'])

        data = r.json()
        self.active = data['active']
        self.status = data['status']
        self.public_address = data['public_address']

    def address(self):
        if self.public_address is None:
            return None
        else:
            return self.public_address['address']
