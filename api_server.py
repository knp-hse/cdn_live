from flask import Flask, request, Response, jsonify
from functools import partial
from exceptions import BroadcastNotFoundException
import validators
import json


def get_api_app(master, tokens):
    def fix(fn):
        return partial(fn, master=master, tokens=tokens)

    api_app = Flask(__name__)
    api_app.add_url_rule('/broadcasts', 'create_broadcast', fix(create_broadcast), methods=['POST'])
    api_app.add_url_rule('/broadcasts/<int:broadcast_id>/viewers', 'get_view_address', fix(get_view_address),
                         methods=['POST'])
    api_app.add_url_rule('/broadcasts/<int:broadcast_id>', 'delete_broadcast', fix(delete_broadcast),
                         methods=['DELETE'])
    api_app.add_url_rule('/broadcasts/<int:broadcast_id>', 'get_broadcast_info', fix(get_broadcast_info),
                         methods=['GET'])
    return api_app


def with_authorize(fn):
    def authorized(*args, **kwargs):
        tokens = kwargs.pop('tokens')
        data = request.get_json()
        if data.get('access_token', None) not in tokens:
            return Response('Access denied', status=401)
        try:
            return fn(data=data, *args, **kwargs)
        except BroadcastNotFoundException:
            return Response(json.dumps({'error': 'Broadcast not found'}), status=404)
    return authorized


@with_authorize
def create_broadcast(master, data):
    url_rtmp, url_hls = data.get('stream_url_rtmp', None), data.get('stream_url_hls', None)
    if url_hls is None and url_rtmp is None:
        return Response("Missing both required urls", status=400)
    if url_rtmp and (not url_rtmp.startswith('rtmp') or not validators.url('http' + url_rtmp[4:])):
        return Response("Invalid url", status=400)
    if url_hls and not validators.url(url_hls):
        return Response("Invalid url", status=400)
    broadcast_id = master.add_broadcast([url_rtmp, url_hls])
    return Response(json.dumps({'broadcast_id': broadcast_id}), status=201,
                    headers={'Content-Type': 'application/json'})


@with_authorize
def get_view_address(master, data, broadcast_id):
    stream_type = data.get('stream_type', None)
    if not stream_type or stream_type not in ('hls', 'rtmp'):
        return Response("Invalid request", status=400)
    addr = master.get_address(broadcast_id, stream_type)
    if addr is None:
        return Response("Broadcast is busy", status=503)
    return Response(json.dumps({'view_url': addr}), status=200, headers={'Content-Type': 'application/json'})


@with_authorize
def delete_broadcast(master, data, broadcast_id):
    master.delete_broadcast(broadcast_id)
    return Response(status=204)


@with_authorize
def get_broadcast_info(master, data, broadcast_id):
    return jsonify(status=master.get_status(broadcast_id))

